<ADSWorkspace Revision="91" Version="100">
    <Workspace Name="">
        <Library Name="ads_behavioral" />
        <Library Name="ads_common_cmps" />
        <Library Name="ads_datacmps" />
        <Library Name="ads_designs" />
        <Library Name="ads_rflib" />
        <Library Name="ads_schematic_layers" />
        <Library Name="ads_simulation" />
        <Library Name="ads_sources" />
        <Library Name="ads_standard_layers" />
        <Library Name="ads_textfonts" />
        <Library Name="ads_tlines" />
        <Library Name="adstechlib" />
        <Library Name="UHF_antenna_lib" />
        <Preferences Name="layout.prf" />
        <Preferences Name="schematic.prf" />
        <Substrate Name="UHF_antenna_lib:RO4003.subst" />
        <Substrate Name="UHF_antenna_lib:RO4003_MS.subst" />
        <Text Name="out.txt" />
        <Log Name="readegs.log" />
        <Log Name="netlist.log" />
        <Log Name="momServer.log" />
        <Folder Name="1. Rect Patch Lin Pol">
            <Folder Name="1. Rect Patch">
                <Cell Name="UHF_antenna_lib:rect_patch_central_feed" />
                <Cell Name="UHF_antenna_lib:rect_patch_central_feed_symb" />
                <Data_Display Name="rect_patch_central_feed.dds" />
                <Data_Display Name="rect_patch_central_feed_symb_MomUW.dds" />
                <Cell Name="UHF_antenna_lib:ImpedanceAsFunctionOfFeedPosition" />
                <Data_Display Name="ImpedanceAsFunctionOfFeedPosition.dds" />
                <Cell Name="UHF_antenna_lib:rect_patch_central_feed_2_patches" />
            </Folder>
            <Folder Name="2. Rect Patch finite gnd">
                <Cell Name="UHF_antenna_lib:rect_patch_central_feed_finite_gnd" />
                <Cell Name="UHF_antenna_lib:rect_patch_central_feed_finite_gnd_symb" />
                <Data_Display Name="rect_patch_central_feed_finite_gnd.dds" />
            </Folder>
            <Folder Name="3. Rect Patch MLIN feed">
                <Cell Name="UHF_antenna_lib:rect_patch_MLIN_feed" />
                <Data_Display Name="rect_patch_MLIN_feed.dds" />
                <Cell Name="UHF_antenna_lib:rect_patch_MLIN_feed_symb" />
                <Data_Display Name="rect_patch_MLIN_feed_symb_MomUW.dds" />
            </Folder>
        </Folder>
        <Folder Name="2. Circular Pol">
            <Folder Name="2. Square Patch 2 feeds">
                <Cell Name="UHF_antenna_lib:square_patch_circ_pol_2feeds" />
                <Cell Name="UHF_antenna_lib:square_patch_circ_pol_2feeds_symb" />
                <Data_Display Name="square_patch_circ_pol_2feeds.dds" />
            </Folder>
            <Folder Name="1. Almost Square Patch 1 feed">
                <Cell Name="UHF_antenna_lib:rect_patch_circ_pol_1feed" />
                <Cell Name="UHF_antenna_lib:rect_patch_circ_pol_1feed_symb" />
                <Data_Display Name="rect_patch_circ_pol_1feed_symb_MomUW.dds" />
                <Data_Display Name="rect_patch_circ_pol_1feed.dds" />
            </Folder>
        </Folder>
        <Folder Name="3. Size Reduction with Short">
            <Folder Name="1. Rect Patch cond wall">
                <Cell Name="UHF_antenna_lib:rect_patch_cond_wall" />
                <Cell Name="UHF_antenna_lib:rect_patch_cond_wall_symb" />
                <Data_Display Name="rect_patch_cond_wall_symb_MomUW.dds" />
                <Data_Display Name="rect_patch_cond_wall.dds" />
                <Cell Name="UHF_antenna_lib:ImpedanceAsFunctionOfFeedPosition_CondWall" />
            </Folder>
            <Folder Name="2. Rect Patch via gnd">
                <Cell Name="UHF_antenna_lib:rect_patch_via_gnd_symb" />
                <Cell Name="UHF_antenna_lib:rect_patch_via_gnd" />
                <Data_Display Name="rect_patch_via_gnd_symb_MomUW.dds" />
                <Data_Display Name="rect_patch_via_gnd.dds" />
            </Folder>
        </Folder>
        <Folder Name="4. U-slotted Rect Patch">
            <Cell Name="UHF_antenna_lib:u_slotted_patch_symb" />
            <Cell Name="UHF_antenna_lib:u_slotted_patch" />
            <Data_Display Name="u_slotted_patch_symb_MomUW.dds" />
            <Data_Display Name="u_slotted_patch.dds" />
        </Folder>
        <Folder Name="5. Butterfly">
            <Cell Name="UHF_antenna_lib:butterfy_symb" />
            <Cell Name="UHF_antenna_lib:butterfy_symb_v2" />
        </Folder>
        <Data_Display Name="EMFarFieldCut.dds" />
        <Data_Display Name="ImpedanceAsFunctionOfFeedPosition_CondWall.dds" />
        <Library Name="empro_standard_layers" />
        <Library Name="ads_standard_layers_ic" />
        <Library Name="ads_schematic_layers_ic" />
        <Library Name="ads_schematic_ports_ic" />
        <Library Name="ads_bondwires" />
        <Library Name="ads_verification_test_bench" />
        <Data_Display Name="rect_patch_central_feed_2_patches_MomUW.dds" />
        <Log Name="search_history.log" />
        <Preferences Name="ads_rflib_lay.prf" />
        <Preferences Name="UHF_antenna_lib_lay.prf" />
        <LibraryDefs Name="lib.defs" />
        <ConfigFile Name="dds.cfg" />
        <ConfigFile Name="de_sim.cfg" />
        <ConfigFile Name="hpeesofsim.cfg" />
        <ConfigFile Name="linecalc.cfg" />
        <Layer_Preference Name="ads_standard_layers.layerprf" />
        <Cell Name="UHF_antenna_lib:test" />
        <Dataset Name="u_slotted_patch_symb_MomUW_a.ds" />
        <Data_Display Name="u_slotted_patch_symb.dds" />
        <Dataset Name="u_slotted_patch_symb_MomUW.ds" />
    </Workspace>
</ADSWorkspace>
