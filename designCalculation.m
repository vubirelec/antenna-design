%// Design of the patch antenna
CenterFreq  = 2.4e9
EpsilonRel  = 3.38
Thickness   = 1.524e-3
c0          = 3e8

Lambda0     = c0/CenterFreq
Lambda      = Lambda0/sqrt(EpsilonRel)

Dim_A       = 0.5*Lambda0
% Dim_B48     = 0.48*Lambda  %// was 0.48

Z_0         = Thickness*pi*120/Dim_A/sqrt(EpsilonRel)

F_Res_Eps0  = c0/Lambda0
F_Res_EpsRel= c0/Lambda

% F_Res_B     = c0 / (2*Dim_B48)


fprintf('============================== BALANIS =================================\n')
W_Balanis           = c0/(2*CenterFreq)*sqrt(2/(EpsilonRel+1))
EpsilonEff_Balanis  = (EpsilonRel+1)/2 + (EpsilonRel-1)/2/sqrt(1+12*Thickness/W_Balanis)
DeltaL_Balanis      = 0.412*Thickness*  ( (EpsilonEff_Balanis+0.3)*(W_Balanis/Thickness+0.264) )/...
                                        ( (EpsilonEff_Balanis-0.258)*(W_Balanis/Thickness+0.8) )
L0_Balanis          = c0 / (2*CenterFreq*sqrt(EpsilonEff_Balanis))
Le_Balanis          = L0_Balanis - 2*DeltaL_Balanis
fprintf('========================================================================\n')

%// Correction of the Width usign the effective length
EpsilonEff_Balanis  = (EpsilonRel+1)/2 + (EpsilonRel-1)/2/sqrt(1+12*Thickness/W_Balanis)
DeltaW_Balanis      = 0.412*Thickness*  ( (EpsilonEff_Balanis+0.3)*(Dim_A/Thickness+0.264) )/...
                                        ( (EpsilonEff_Balanis-0.258)*(Dim_A/Thickness+0.8) )
W_Balanis           = Dim_A + 2*DeltaW_Balanis

fprintf('=============================== POSITION ==============================\n')
Gt                  = ((W_Balanis/Lambda0).^2)/90
Rin                 = 50
Pos_Balanis         = asin(1- Rin*Gt*2)/pi*L0_Balanis
Rel_Pos_Balanis     = Pos_Balanis/L0_Balanis
